<?php

namespace Core;

class Paginator
{
	private $_aData;
	protected $_nMaxElementsForPage;
	protected $_nTotPages;

	function __construct($aData=array())
	{
		if(!empty($aData))
			$this->_aData = $aData;
	}

	public function render($nPage)
	{
		if($nPage>$this->getTotPages())
			return false;
		$aRet = array();
		for($i=$nPage*$this->_nMaxElementsForPage; $i<(($nPage*$this->_nMaxElementsForPage)+$this->_nMaxElementsForPage);$i++)
			$aRet[] = $this->_aData[$i];
		return $aRet;	
	}

	public function getTotPages()
	{
		$nTot = count($this->_aData)/$this->_nMaxElementsForPage;
		$nExtra = count($this->_aData)%$this->_nMaxElementsForPage;
		$this->_nTotPages = (int)$nTot;
		if($nExtra>0)
			$this->_nTotPages++;
		return $this->_nTotPages;
	}

	public function setMaxElementsForPage($nElements)
	{
		$this->_nMaxElementsForPage = $nElements;
	}

	public function getMaxElementsForPage()
	{
		return $this->_nMaxElementsForPage;
	}
}
