module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		phpunit: {
			classes: {
				dir: 'test/src/*'
			},
			options: {
				bin: 'vendor/bin/phpunit',
				bootstrap: 'vendor/autoload.php',
				colors: true
			}
		}
	});

	grunt.loadNpmTasks('grunt-phpunit');
	grunt.registerTask('default', ['phpunit']);
};