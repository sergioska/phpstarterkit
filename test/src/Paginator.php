<?php

namespace Test;

use Core\Paginator;

class PaginatorTest extends \PHPUnit_Framework_TestCase
{

	public function testRender()
	{
		$aData = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
		$paginator = new Paginator($aData);
		$paginator->setMaxElementsForPage(5);
		// check page 0
		$this->assertEquals(array(1,2,3,4,5), $paginator->render(0));
		// check page 1
		$this->assertEquals(array(6,7,8,9,10), $paginator->render(1));
		// check a page out of range
		$this->assertEquals(false, $paginator->render(5));
	}

	public function testSetMaxElementsForPage()
	{	
		$aData = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
		$paginator = new Paginator($aData);
		$paginator->setMaxElementsForPage(5);
		$this->assertEquals(5, $paginator->getMaxElementsForPage());
	}

	public function testGetTotPages()
	{
		$aData = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
		$paginator = new Paginator($aData);
		$paginator->setMaxElementsForPage(3);
		$this->assertEquals(7, $paginator->getTotPages());
	}
	
}