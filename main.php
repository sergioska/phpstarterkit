<?php

require_once('vendor/autoload.php');

use Core\Paginator;

$aData = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

$oPaginator = new Paginator($aData);
$oPaginator->setMaxElementsForPage(5);
var_dump($oPaginator->render(1));

